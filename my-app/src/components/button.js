import React from 'react'
import { useDispatch } from 'react-redux'
import {machin, clickOnName} from '../app/actions'



export default function Buttons(){
    const dispatch = useDispatch()  
    return <div>
        
                <button onClick={()=>dispatch({type:machin})}>machin</button>
                <button onClick={()=>dispatch({type:"truc"})}>truc</button>
                <button onClick={()=>dispatch({type:"reset"})}>reset</button>

                <button onClick={()=>dispatch({type:"increment"})}>+</button>
                <button>-</button>
            </div>
}
