import React from 'react'
import {useSelector} from 'react-redux'


export default function Display(){
    const string = useSelector(state=>state.string)

    return <div>
        <p>string:{string}</p>
        <p>first number: {useSelector(state=>state.number.first)}</p>

    </div>
}